﻿using System;
using System.Collections.Generic;
using TeacherManagement;
using static System.Runtime.InteropServices.JavaScript.JSType;

class Program
{
    static void Main(string[] args)
    {
        int soLuongGiaoVien = InputIntNumber(1, int.MaxValue, "Nhap so luong giao vien: ", "So luong khong hop le");

        List<GiaoVien> danhSachGiaoVien = new List<GiaoVien>();
        for (int i = 0; i < soLuongGiaoVien; i++)
        {
            Console.WriteLine($"\nNhap thong tin cho giao vien {i + 1}:");
            string hoTen = InputString("Ho ten: ", "Ho ten khong hop le", @"[A-Za-z]");

            int namSinh = InputIntNumber(1900, DateTime.Now.Year, "Nam sinh: ", "Nam sinh khong hop le");

            double luongCoBan = InputDoubleNumber(1, double.MaxValue, "Luong co ban: ", "Luong co ban khong hop le");

            double heSoLuong = InputDoubleNumber(1, double.MaxValue, "He so luong: ", "He so luong khong hop le");

            GiaoVien giaoVien = new GiaoVien(hoTen, namSinh, luongCoBan, heSoLuong);
            danhSachGiaoVien.Add(giaoVien);
        }
        if (danhSachGiaoVien.Count > 0)
        {
            GiaoVien giaoVienLuongThapNhat = danhSachGiaoVien[0];
            foreach (var giaoVien in danhSachGiaoVien)
            {
                if (giaoVien.TinhLuong() < giaoVienLuongThapNhat.TinhLuong())
                {
                    giaoVienLuongThapNhat = giaoVien;
                }

            }
            Console.WriteLine("\nThong tin giao vien co luong thap nhat:");
            giaoVienLuongThapNhat.XuatThongTin();
            Console.Read();
        }
        else
        {
            Console.WriteLine("Khong co giao vien nao trong danh sach.");
        }

    }

    public static string InputString(string msg, string errMsg, string regex)
    {
        string output = "";
        bool flag = false;
        while (!flag)
        {
            try
            {
                Console.Write(msg);
                output = Console.ReadLine();
                if (!System.Text.RegularExpressions.Regex.IsMatch(output, regex))
                {
                    throw new Exception();
                }
                flag = true;
            }
            catch (Exception)
            {
                Console.WriteLine(errMsg);
            }
        }
        return output;
    }

    public static int InputIntNumber(int min, int max, string msg, string errMsg)
    {
        int n = 0;
        bool flag = false;
        while (!flag)
        {
            try
            {
                Console.Write(msg);
                string input = Console.ReadLine();
                if (!int.TryParse(input, out n) || n < min || n > max)
                {
                    throw new Exception();
                }
                flag = true;
            }
            catch (Exception)
            {
                Console.WriteLine(errMsg);
            }
        }
        return n;
    }


    public static double InputDoubleNumber(double min, double max, string msg, string errMsg)
    {
        double n = 0;
        bool flag = false;
        while (!flag)
        {
            try
            {
                Console.Write(msg);
                string input = Console.ReadLine();
                if (!double.TryParse(input, out n) || n < min || n > max)
                {
                    throw new Exception();
                }
                flag = true;
            }
            catch (Exception)
            {
                Console.WriteLine(errMsg);
            }
        }
        return n;
    }


}